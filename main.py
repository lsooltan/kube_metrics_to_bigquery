import os
import datetime
from google.cloud import monitoring_v3
from google.cloud import bigquery

def list_metrics(metric_type):
    """
    Lists metrics from Google Cloud Monitoring based on the specified metric type.
    """
    project_id = os.environ.get("PROJECT_ID")
    project_name = f"projects/{project_id}"

    client = monitoring_v3.MetricServiceClient()

    interval = monitoring_v3.TimeInterval({
        "end_time": datetime.datetime.utcnow(),
        "start_time": datetime.datetime.utcnow() - datetime.timedelta(minutes=60),
    })

    results = client.list_time_series(
        request={
            "name": project_name,
            "filter": f'metric.type="{metric_type}"',
            "interval": interval,
            "view": monitoring_v3.ListTimeSeriesRequest.TimeSeriesView.FULL,
        }
    )

    data = []
    for result in results:
        for point in result.points:
            data.append({
                "timestamp": point.interval.end_time.isoformat(),
                "metric_type": metric_type,
                "resource_id": result.resource.labels["pod_id"],  # Adjust as per actual label
                "value": point.value.double_value  # Assuming it's a float value
            })

    return data

def insert_into_bigquery(dataset_id, table_id, rows):
    """
    Inserts the given rows into a BigQuery table.
    """
    project_id = os.environ.get("PROJECT_ID")
    client = bigquery.Client(project=project_id)
    table_ref = client.dataset(dataset_id).table(table_id)
    table = client.get_table(table_ref)

    errors = client.insert_rows_json(table, rows)
    if errors:
        print("Errors occurred while inserting rows: {}".format(errors))
    else:
        print("Data inserted successfully.")

# Main execution
if __name__ == "__main__":
    # Replace these variables with your specific identifiers
    dataset_id = "Finops"
    table_id = "monitoringpods"

    # Metrics types
    cpu_metric_type = "kubernetes.io/container/cpu/usage_time"
    memory_metric_type = "kubernetes.io/container/memory/used_bytes"

    # Fetch and insert CPU metrics
    cpu_metrics_data = list_metrics(cpu_metric_type)
    insert_into_bigquery(dataset_id, table_id, cpu_metrics_data)

    # Fetch and insert Memory metrics
    memory_metrics_data = list_metrics(memory_metric_type)
    insert_into_bigquery(dataset_id, table_id, memory_metrics_data)
